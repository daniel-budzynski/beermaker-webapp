package com.beermakerbrewery.beermakerwebapp.enums;

public enum ProcessStepType {
MASHING,
BOILING,
HOPPING
}
