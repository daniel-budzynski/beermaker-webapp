package com.beermakerbrewery.beermakerwebapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.beermakerbrewery.beermakerwebapp.entity.Role;
import com.beermakerbrewery.beermakerwebapp.entity.User;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RoleRepository;
import com.beermakerbrewery.beermakerwebapp.repository.UserRepository;

@Service
@Transactional 
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	public List<User> findAll() {
		return userRepository.findAll();
		
	}

	public void save(User user) {
		user.setEnabled(true);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));
		
		List<Role> roles = new ArrayList<Role>();
		roles.add(roleRepository.findByName("ROLE_USER"));
		user.setRoles(roles);
		
		userRepository.save(user);
		
	}

	public User findOne(int id) {
		return userRepository.findOne(id);
	}

	public Object findOne(String name) {
		User user = userRepository.findByName(name);
		return findOne(user.getId());
	}
}
