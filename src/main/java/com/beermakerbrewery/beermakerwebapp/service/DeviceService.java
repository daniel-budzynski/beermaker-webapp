package com.beermakerbrewery.beermakerwebapp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.beermakerbrewery.beermakerwebapp.entity.Chart;
import com.beermakerbrewery.beermakerwebapp.entity.ChartItem;
import com.beermakerbrewery.beermakerwebapp.entity.Device;
import com.beermakerbrewery.beermakerwebapp.entity.Recipe;
import com.beermakerbrewery.beermakerwebapp.entity.RecipeItem;
import com.beermakerbrewery.beermakerwebapp.entity.RecipeProcessStep;
import com.beermakerbrewery.beermakerwebapp.repository.ChartItemRepository;
import com.beermakerbrewery.beermakerwebapp.repository.ChartRepository;
import com.beermakerbrewery.beermakerwebapp.repository.DeviceRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeItemRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeProcessStepRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeRepository;

@Service
public class DeviceService {
	
	@Autowired
	private RecipeRepository recipeRepository;
	
	@Autowired
	private RecipeItemRepository recipeItemRepository;
	
	@Autowired
	private RecipeProcessStepRepository recipeProcessStepRepository;
	
	@Autowired
	private DeviceRepository deviceRepository;
	
	@Autowired
	private ChartRepository chartRepository;
	
	@Autowired 
	private ChartItemRepository chartItemRepository;
	
	public List<Device> findAll() {
		Page<Device> page = deviceRepository.findAll(new PageRequest(0, 20, Direction.ASC, "id"));
		return page.getContent();
		
	}
	@Transactional
	public List<Device> findAllWithDetails() {
		List<Device> devices =  deviceRepository.findAll();
			for(Device device : devices){
				List<Chart> charts = chartRepository.findByDevice(device);
				device.setCharts(charts);
				for(Chart chart : charts){
					List<ChartItem> chartItems = chartItemRepository.findBychart(chart);
					chart.setChartItems(chartItems);
				}
			}			
		return devices;
	}

	public Recipe findOne(int id) {
		return recipeRepository.findOne(id);
	}
	
	@Transactional
	public Recipe findOneWithDetails(int id) {
			Recipe recipe = findOne(id);
				List<RecipeItem> recipeItems = recipeItemRepository.findByRecipe(recipe);
				recipe.setRecipeItems(recipeItems);
		return recipe;
	}
}
