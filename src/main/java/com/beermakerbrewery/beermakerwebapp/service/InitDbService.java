package com.beermakerbrewery.beermakerwebapp.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.beermakerbrewery.beermakerwebapp.entity.Chart;
import com.beermakerbrewery.beermakerwebapp.entity.ChartItem;
import com.beermakerbrewery.beermakerwebapp.entity.Device;
import com.beermakerbrewery.beermakerwebapp.entity.Ingredient;
import com.beermakerbrewery.beermakerwebapp.entity.ProcessStep;
//import com.beermakerbrewery.beermakerwebapp.entity.Blog;
//import com.beermakerbrewery.beermakerwebapp.entity.Item;
import com.beermakerbrewery.beermakerwebapp.entity.Recipe;
import com.beermakerbrewery.beermakerwebapp.entity.RecipeItem;
import com.beermakerbrewery.beermakerwebapp.entity.RecipeProcessStep;
import com.beermakerbrewery.beermakerwebapp.entity.Role;
import com.beermakerbrewery.beermakerwebapp.entity.User;
import com.beermakerbrewery.beermakerwebapp.enums.ProcessStepType;
import com.beermakerbrewery.beermakerwebapp.repository.ChartItemRepository;
import com.beermakerbrewery.beermakerwebapp.repository.ChartRepository;
import com.beermakerbrewery.beermakerwebapp.repository.DeviceRepository;
//import com.beermakerbrewery.beermakerwebapp.repository.BlogRepository;
//import com.beermakerbrewery.beermakerwebapp.repository.ItemRepository;
import com.beermakerbrewery.beermakerwebapp.repository.IngredientRepository;
import com.beermakerbrewery.beermakerwebapp.repository.ProcessStepRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeItemRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeProcessStepRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RecipeRepository;
import com.beermakerbrewery.beermakerwebapp.repository.RoleRepository;
import com.beermakerbrewery.beermakerwebapp.repository.UserRepository;

@Transactional
@Service
public class InitDbService {
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RecipeRepository recipeRepository;
	
	@Autowired
	private IngredientRepository ingredientRepository;
	
	@Autowired
	private RecipeItemRepository recipeItemRepository;
	
	@Autowired
	private ProcessStepRepository processStepRepository;
	
	@Autowired
	private RecipeProcessStepRepository recipeProcessStepRepository; 
	
	@Autowired
	private ChartItemRepository chartItemRepository;
	
	@Autowired
	private ChartRepository chartRepository;
	
	@Autowired
	private DeviceRepository deviceRepository;
	
	@PostConstruct
	public void init(){
		
		
		Role roleUser = new Role();
		roleUser.setName("ROLE_USER");
		roleRepository.save(roleUser);
		
		Role roleAdmin = new Role();
		roleAdmin.setName("ROLE_ADMIN");
		roleRepository.save(roleAdmin);
		
		
		
		Device device = new Device();
		Chart chart = new Chart();
		chart.setName("FirstChart");
		
		device.setMACAdress("MG-DB-12-99-00-55-23");
		
		deviceRepository.save(device);
		List<Chart> charts = new ArrayList<Chart>();
		charts.add(chart);
		device.setCharts(charts);
		chart.setDevice(device);
		chartRepository.save(chart);
		//chart.setDevice(device);
		
		
		User userAdmin = new User();
		userAdmin.setName("admin");
		userAdmin.setPassword(new BCryptPasswordEncoder().encode("budyn1337"));
		userAdmin.setEnabled(true);
		List<Role> roles = new ArrayList<Role>();
		roles.add(roleAdmin);
		roles.add(roleUser);
		userAdmin.setRoles(roles);
		List<Device> devices = new ArrayList<Device>();
		devices.add(device);
		userAdmin.setDevices(devices);
		userRepository.save(userAdmin);
		
		
		
		User userTest = new User();
		userTest.setName("testuser");
		userTest.setPassword(new BCryptPasswordEncoder().encode("test"));
		userTest.setEnabled(true);
		List<Role> roles1 = new ArrayList<Role>();
		roles1.add(roleUser);
		userTest.setRoles(roles1);
		userRepository.save(userTest);
		
		
		
		Chart chart1 = new Chart();
		chart1.setDevice(device);
		chart1.setName("FirstChart");
		chartRepository.save(chart1);
		
		
		Ingredient ingredient = new Ingredient();
		ingredient.setName("S�od J�czmienny");
		ingredientRepository.save(ingredient);
		
		Ingredient ingredient1 = new Ingredient();
		ingredient1.setName("S�od Karmelowy");
		ingredientRepository.save(ingredient1);
		
		Ingredient ingredient2 = new Ingredient();
		ingredient2.setName("Chmiel Marynka");
		ingredientRepository.save(ingredient2);
		
		Ingredient ingredient3 = new Ingredient();
		ingredient3.setName("Chmiel Citra");
		ingredientRepository.save(ingredient3);
		
		ProcessStep processStep = new ProcessStep();
		processStep.setType(ProcessStepType.MASHING);
		processStep.setLevel(0);
		
		ProcessStep processStep2 = new ProcessStep();
		processStep2.setType(ProcessStepType.MASHING);
		processStep2.setLevel(1);
		
		ProcessStep processStep3 = new ProcessStep();
		processStep3.setType(ProcessStepType.BOILING);
		processStep3.setLevel(0);
		
		ProcessStep processStep4 = new ProcessStep();
		processStep4.setType(ProcessStepType.BOILING);
		processStep4.setLevel(1);
		
		ProcessStep processStep5 = new ProcessStep();
		processStep5.setType(ProcessStepType.HOPPING);
		processStep5.setLevel(0);
		
		ProcessStep processStep6 = new ProcessStep();
		processStep6.setType(ProcessStepType.HOPPING);
		processStep6.setLevel(1);
		
		ProcessStep processStep7 = new ProcessStep();
		processStep7.setType(ProcessStepType.HOPPING);
		processStep7.setLevel(2);
		
		ProcessStep processStep8 = new ProcessStep();
		processStep8.setType(ProcessStepType.HOPPING);
		processStep8.setLevel(3);
		
		ProcessStep processStep9 = new ProcessStep();
		processStep9.setType(ProcessStepType.HOPPING);
		processStep9.setLevel(4);
		
		for(int i = 0; i < 20; i++){
		
		
/*		Blog blogJavavids = new Blog();
		blogJavavids.setName("JavaVids");
		blogJavavids.setUrl("http://feeds.feedburner.com/javavids?format=xml");
		blogJavavids.setUser(userAdmin);
		blogRepository.save(blogJavavids);
		
		Item item1 = new Item();
		item1.setBlog(blogJavavids);
		item1.setTitle("first");
		item1.setLink("http://www.javavids.com");
		item1.setPublishedDate(new Date());
		itemRepository.save(item1);
		
		Item item2 = new Item();
		item2.setBlog(blogJavavids);
		item2.setTitle("second");
		item2.setLink("http://www.javavids.com");
		item2.setPublishedDate(new Date());
		itemRepository.save(item2);
*/		

		

		
		Recipe recipe = new Recipe();
		recipe.setName("�ywiec APA " + i);
		recipe.setDescription("Sw�j aromat i smak zawdzi�cza zastosowaniu pi�ciu odmian chmielu oraz warzeniu w otwartych kadziach. Piwo to smakuje najlepiej, podawane w temperaturze 9-11 stopni Celsjusza.");
		recipe.setStyle("American Pale Ale");
		recipe.setImageName("image1");
		recipe.setImageExtension("png");
		RecipeItem recipeItem = new RecipeItem();
		recipeItem.setIngredient(ingredient);
		recipeItem.setQuantity(10.00);
		RecipeProcessStep recipeProcessStep = new RecipeProcessStep();
		recipeProcessStep.setProcessStep(processStep5);
		recipeProcessStep.setIngredient(ingredient2);
		recipeProcessStep.setQuantity(20.0);
		recipeProcessStep.setTime(30);
		
		
		RecipeItem recipeItem1 = new RecipeItem();
		recipeItem1.setIngredient(ingredient2);
		recipeItem1.setQuantity(15.00);
		
//		List<RecipeItem> recipeItems = new ArrayList<RecipeItem>();
//		recipeItems.add(recipeItem);
//		recipeItems.add(recipeItem1);
//		recipe.setRecipeItems(recipeItems);
		
		
		Recipe recipe1 = new Recipe();
		recipe1.setName("Jaki� Ko�lak " + i);
		recipe1.setDescription("Ko�lak (niem. Bock) znany jest od XVI w. Sw�j rodow�d wywodzi z dolnosakso�skiego miasta Einbeck, gdzie od XIII w. warzono mocne piwo Starkbier. By�o ono w�wczas piwem g�rnej fermentacji eksportowanym do wielu pa�stw.");
		recipe1.setStyle("Bock");
		recipe1.setImageName("image2");
		recipe1.setImageExtension("png");
		
		RecipeItem recipeItem2 = new RecipeItem();
		recipeItem2.setIngredient(ingredient1);
		recipeItem2.setQuantity(20.00);
		
		

		
		RecipeItem recipeItem3 = new RecipeItem();
		recipeItem3.setIngredient(ingredient3);
		recipeItem3.setQuantity(25.00);
		
		
//		List<RecipeItem> recipeItems1 = new ArrayList<RecipeItem>();
//		recipeItems1.add(recipeItem2);
//		recipeItems1.add(recipeItem3);
//		recipe1.setRecipeItems(recipeItems1);

		recipeRepository.save(recipe);
		recipeRepository.save(recipe1);
		recipeItem.setRecipe(recipe);
		recipeItem1.setRecipe(recipe);
		recipeItem2.setRecipe(recipe1);
		recipeItem3.setRecipe(recipe1);
		recipeItemRepository.save(recipeItem);
		recipeItemRepository.save(recipeItem1);
		recipeItemRepository.save(recipeItem2);
		recipeItemRepository.save(recipeItem3);
		
		recipe = null;
		recipe1 = null;
		
		
		ChartItem chartItem = new ChartItem();
		chartItem.setChart(chart);
		chartItem.setTemperature(20.5+i);
		chartItemRepository.save(chartItem);
		
		ChartItem chartItem1 = new ChartItem();
		chartItem1.setChart(chart1);
		chartItem1.setTemperature(50.5+i);
		chartItemRepository.save(chartItem1);
	}
	}

}
