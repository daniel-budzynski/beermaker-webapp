package com.beermakerbrewery.beermakerwebapp.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Ingredient {

	@Id
	@GeneratedValue
	private Integer id;

	private String name;
	
	private String type;
	
	private String uom;

	private String imageName;

	private String imageExtension;
	
	@OneToMany(mappedBy="ingredient")
	private List<RecipeItem> recipeItems;
	
	@OneToMany(mappedBy="ingredient")
	private List<RecipeProcessStep> recipeProcessSteps;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageExtension() {
		return imageExtension;
	}

	public void setImageExtension(String imageExtension) {
		this.imageExtension = imageExtension;
	}

	public List<RecipeItem> getRecipeItems() {
		return recipeItems;
	}

	public void setRecipeItems(List<RecipeItem> recipeItems) {
		this.recipeItems = recipeItems;
	}

	public List<RecipeProcessStep> getRecipeProcessSteps() {
		return recipeProcessSteps;
	}

	public void setRecipeProcessSteps(List<RecipeProcessStep> recipeProcessSteps) {
		this.recipeProcessSteps = recipeProcessSteps;
	}

}
