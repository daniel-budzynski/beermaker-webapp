package com.beermakerbrewery.beermakerwebapp.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.beermakerbrewery.beermakerwebapp.enums.ProcessStepType;

@Entity
public class ProcessStep {
	
	@Id
	@GeneratedValue
	private Integer id;
	
	private ProcessStepType type ;
	
	private int level;
	
	@OneToMany(mappedBy="processStep")
	private List<RecipeProcessStep> recipeProcessSteps;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ProcessStepType getType() {
		return type;
	}

	public void setType(ProcessStepType type) {
		this.type = type;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}
	
	public List<RecipeProcessStep> getRecipeProcessSteps() {
		return recipeProcessSteps;
	}

	public void setRecipeProcessSteps(List<RecipeProcessStep> recipeProcessSteps) {
		this.recipeProcessSteps = recipeProcessSteps;
	}

	
	
	
	 
	

}


