package com.beermakerbrewery.beermakerwebapp.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Device {

	@Id
	@GeneratedValue
	private Integer id;
	
	private String MACAdress;
	
	@OneToMany(mappedBy="device")
	private List<Chart> charts;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMACAdress() {
		return MACAdress;
	}
	public void setMACAdress(String mACAdress) {
		MACAdress = mACAdress;
	}
	@JsonManagedReference
	public List<Chart> getCharts() {
		return charts;
	}
	public void setCharts(List<Chart> charts) {
		this.charts = charts;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
}
