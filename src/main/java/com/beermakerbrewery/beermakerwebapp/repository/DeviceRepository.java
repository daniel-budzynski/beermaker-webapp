package com.beermakerbrewery.beermakerwebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beermakerbrewery.beermakerwebapp.entity.Device;

public interface DeviceRepository extends JpaRepository<Device, Integer> {

}
