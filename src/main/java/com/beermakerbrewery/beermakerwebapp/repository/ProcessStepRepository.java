package com.beermakerbrewery.beermakerwebapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beermakerbrewery.beermakerwebapp.entity.ProcessStep;

public interface ProcessStepRepository extends JpaRepository<ProcessStep, Integer>{

}
