package com.beermakerbrewery.beermakerwebapp.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beermakerbrewery.beermakerwebapp.entity.Recipe;
import com.beermakerbrewery.beermakerwebapp.entity.RecipeProcessStep;

public interface RecipeProcessStepRepository extends JpaRepository<RecipeProcessStep, Serializable> {

	List<RecipeProcessStep> findByRecipe(Recipe recipe);

}
