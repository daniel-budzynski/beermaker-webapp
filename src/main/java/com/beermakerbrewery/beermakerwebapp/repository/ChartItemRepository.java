package com.beermakerbrewery.beermakerwebapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beermakerbrewery.beermakerwebapp.entity.Chart;
import com.beermakerbrewery.beermakerwebapp.entity.ChartItem;

public interface ChartItemRepository extends JpaRepository<ChartItem, Integer> {

	List<ChartItem> findBychart(Chart chart);

}
