package com.beermakerbrewery.beermakerwebapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.beermakerbrewery.beermakerwebapp.entity.Chart;
import com.beermakerbrewery.beermakerwebapp.entity.Device;

public interface ChartRepository extends JpaRepository<Chart, Integer> {

	List<Chart> findByDevice(Device device);

}
