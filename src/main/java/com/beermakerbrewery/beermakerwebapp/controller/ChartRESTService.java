package com.beermakerbrewery.beermakerwebapp.controller;


import com.beermakerbrewery.beermakerwebapp.entity.ChartItem;
import com.beermakerbrewery.beermakerwebapp.entity.Device;
import com.beermakerbrewery.beermakerwebapp.entity.Chart;
import com.beermakerbrewery.beermakerwebapp.repository.ChartItemRepository;
import com.beermakerbrewery.beermakerwebapp.service.DeviceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.SecureRandom;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/chart")
public class ChartRESTService {
	
	@Autowired
	private ChartItemRepository chartItemRepository;
	@Autowired
	private DeviceService deviceService;
	
    static Set Charts;

    @RequestMapping(value = "/{chartId}", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public ChartItem getFoobar(@PathVariable int chartId) {
        return chartItemRepository.findOne(chartId);
    }

    @RequestMapping(value = "/htmllist", method = RequestMethod.GET, headers = "Accept=text/html", produces = {"text/html"})
    @ResponseBody
    public String getFoobarListHTML() {
        String retVal = "<html><body><table border=1>";
        Iterator X = Charts.iterator();
        while (X.hasNext()) {
        	ChartItem f = (ChartItem) X.next();
            retVal += "<tr><td>" + f.getId() + "</td><td>" + f.getTimestamp() + "</td><td>" + f.getTemperature() + "</td></tr>";
        }
        retVal += "</table></body></html>";

        return retVal;
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public List<ChartItem> getFoobarList() {
        //return Charts;
    	return chartItemRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, headers = "Accept=application/json", produces = {"application/json", "application/xml"})
    @ResponseBody
    public List<Device> getFoobars() {
        return deviceService.findAllWithDetails();
    }

    @RequestMapping(value = "/{chartId}", method = RequestMethod.PUT, headers = "Accept=application/json", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public Chart editFoobar(@RequestBody Chart foobar, @PathVariable int chartId) {
        Iterator X = Charts.iterator();
        while (X.hasNext()) {
        	Chart f = (Chart) X.next();
            if (chartId == f.getId()) {
                f.setId(foobar.getId());
                f.setName(foobar.getName());
                return f;
            }
        }
        return null;
    }

    @RequestMapping(value = "/{chartId}", method = RequestMethod.DELETE, headers = "Accept=application/json", produces = {"application/json"})
    @ResponseBody
    public boolean deleteFoobar(@PathVariable int chartId) {
        System.out.println("Delete call.");
        Iterator fooIterator = Charts.iterator();
        while (fooIterator.hasNext()) {
        	Chart foobar = (Chart) fooIterator.next();
            System.out.println(foobar);
            if (foobar.getId() == chartId) {
                fooIterator.remove();
                return true;
            }
        }
        return false;
    }

    @RequestMapping(method = RequestMethod.POST, headers = "Accept=application/json", produces = {"application/json"}, consumes = {"application/json"})
    @ResponseBody
    public ChartItem createFoobar(@RequestBody ChartItem chartItem) {
        return chartItemRepository.save(chartItem);
    }
}