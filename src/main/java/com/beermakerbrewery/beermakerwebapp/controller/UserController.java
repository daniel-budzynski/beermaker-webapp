package com.beermakerbrewery.beermakerwebapp.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.beermakerbrewery.beermakerwebapp.entity.Role;
import com.beermakerbrewery.beermakerwebapp.entity.User;
import com.beermakerbrewery.beermakerwebapp.repository.RoleRepository;
import com.beermakerbrewery.beermakerwebapp.service.UserService;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	

	
	@ModelAttribute("user")
	public User construct(){
		return new User();
	}
	
	@RequestMapping("/users")
	public String users(Model model) {
		model.addAttribute("users", userService.findAll());
		return "users";
	}
	
	@RequestMapping("/users/{id}")
	public String userDetails(Model model, @PathVariable int id) {
		model.addAttribute("userDetails", userService.findOne(id));
		return "user-details";
	}
	
	@RequestMapping("/my-account")
	public String userDetails(Model model, Principal principal) {
		String name = principal.getName();
		model.addAttribute("userDetails", userService.findOne(name));
		return "user-details";
	}
	
	@RequestMapping("/user-register")
	public String showRegister() {
		return "user-register";
	}
	
	@RequestMapping(value="/user-register", method=RequestMethod.POST)
	public String doRegister(@ModelAttribute("user") User user) {
		userService.save(user);	
		
		return "redirect:/user-register.html?success=true";
	}
	
}
